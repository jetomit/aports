# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=dasel
pkgver=2.1.2
pkgrel=0
pkgdesc="Query and modify data structures using selector strings"
url="https://daseldocs.tomwright.me/"
license="MIT"
arch="all"
makedepends="go"
source="https://github.com/TomWright/dasel/archive/v$pkgver/dasel-$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags "
		-X github.com/tomwright/dasel/internal.Version=$pkgver
		" ./cmd/dasel
}

check() {
	go test ./...
}

package() {
	install -Dm755 dasel -t "$pkgdir"/usr/bin/
}

sha512sums="
0d18a5b225c9e1bb4e49c7a1ee4cfd3d01ef09eca0ae20637469740adad8ba0cf8ba51192da0de5b34821d97314b0b30c6450678f6253cd14aea8a9397aeafce  dasel-2.1.2.tar.gz
"
