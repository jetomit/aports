# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=aws-c-common
pkgver=0.8.15
pkgrel=0
pkgdesc="Core c99 package for AWS SDK for C including cross-platform primitives, configuration, data structures, and error handling"
url="https://github.com/awslabs/aws-c-common"
# s390x: fails tests
arch="all !s390x"
license="Apache-2.0"
makedepends="
	cmake
	python3-dev
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/awslabs/aws-c-common/archive/refs/tags/v$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -j${JOBS:-2}
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev
	amove usr/lib/aws-c-common
}

sha512sums="
533df96e58c4d966e77ee07f320e81695035971638fdcce0a6c5c81130943a47fc2d46b9553864b7e28a59ec1e9869e3c133b010dd003ddeac0926d720a2eb3d  aws-c-common-0.8.15.tar.gz
"
