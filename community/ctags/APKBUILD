# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=ctags
pkgver=6.0.20230319.0
_realver="p$pkgver"
pkgrel=0
pkgdesc="Generator of tags for all types of C/C++ languages"
url="https://ctags.io/"
arch="all"
license="GPL-2.0-or-later"
checkdepends="diffutils python3"
makedepends="autoconf automake pkgconf py3-docutils"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/universal-ctags/ctags/archive/$_realver.tar.gz"
builddir="$srcdir"/$pkgname-$_realver

# secfixes:
#   5.8-r5:
#     - CVE-2014-7204

prepare() {
	default_prepare
	./autogen.sh

	# Fail, likely due to compatibility issues with musl's iconv.
	# Alternative solution: Build ctags with --disable-iconv.
	rm -r Tmain/input-encoding-option.d \
		Tmain/output-encoding-option.d
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-external-sort
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
ad91f442e7b55150e2a5141bd0dad4b35048a5ccd9f4fdefbb8219e22d11b08e939f750446876928e535e385b6969409172fa8ccaaa6d06e2dd69dd5b4556992  ctags-6.0.20230319.0.tar.gz
"
