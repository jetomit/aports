# Maintainer: Eivind Uggedal <eu@eju.no>
pkgname=py3-redis
_pkgname=redis
pkgver=4.5.3
pkgrel=0
pkgdesc="Python3 client for Redis key-value store"
url="https://github.com/andymccurdy/redis-py"
arch="noarch"
license="MIT"
depends="python3 py3-async-timeout py3-hiredis"
makedepends="py3-setuptools py3-gpep517 py3-wheel"
checkdepends="py3-pytest py3-mock redis"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver
options="!check" # tests fail due to old version + disabled proprietary redis features

replaces="py-redis" # Backwards compatibility
provides="py-redis=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	redis-server --dir "$builddir" &
	trap "kill $!" EXIT
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
c148cc8021816f4ddb9fc601bf1b21788f0f7da780384e05c8cc37a3d24713f8f4b5eab2d285b6c2bc999d8fc0b382734f79d9d22d9ca47bf7db6781568f59a9  redis-4.5.3.tar.gz
"
