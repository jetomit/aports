# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=git-machete
pkgver=3.16.2
pkgrel=0
pkgdesc="git repository organizer & rebase/merge workflow automation tool"
url="https://github.com/VirtusLab/git-machete"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-pytest-mock"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/VirtusLab/git-machete/archive/refs/tags/v$pkgver/git-machete-$pkgver.tar.gz
	"

build() {
	python3 setup.py build
}

check() {
	pytest
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	install -Dm0644 "$builddir"/completion/git-machete.completion.bash \
		"$pkgdir/usr/share/bash-completion/completions/git-machete"
	install -Dm0644 "$builddir"/completion/git-machete.completion.zsh \
		"$pkgdir/usr/share/zsh/site-functions/_git-machete"
	install -Dm0644 "$builddir"/completion/git-machete.fish \
		-t "$pkgdir/usr/share/fish/completions"
}

sha512sums="
8168f4860ca5462f4acc2c8bc6459e51be5a20d2de4e2c08b5315ce11436b6ae1191975e9858fe1f0b8a4bf4a3a512f45a358a3828437a49f20abc49d68d1375  git-machete-3.16.2.tar.gz
"
