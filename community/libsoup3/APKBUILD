# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=libsoup3
pkgver=3.4.0
pkgrel=1
pkgdesc="Gnome HTTP client/server Library"
url="https://wiki.gnome.org/Projects/libsoup"
arch="all"
license="LGPL-2.0-or-later"
subpackages="$pkgname-dev $pkgname-lang $pkgname-dbg"
depends="glib-networking gsettings-desktop-schemas"
makedepends="
	brotli>=1.0.9-r1
	gobject-introspection-dev
	libgcrypt-dev
	libgpg-error-dev
	libpsl-dev
	libxml2-dev
	meson
	nghttp2-dev
	sqlite-dev
	vala
	zlib-dev
	"
checkdepends="gnutls-dev"
source="https://download.gnome.org/sources/libsoup/${pkgver%.*}/libsoup-$pkgver.tar.xz
	char-int-force-http.patch
	"
builddir="$srcdir/libsoup-$pkgver"

case "$CARCH" in
x86*)
	;;
*)
	# arm*: sigill for some reason
	# rest: sigabrt, http1 != http2 on localhost req
	options="$options !check"
	;;
esac

build() {
	abuild-meson \
		-Db_lto=true \
		-Dtls_check=false \
		-Dintrospection=enabled \
		-Dvapi=enabled \
		-Dtests="$(want_check && echo true || echo false)" \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test -t 10 --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
d9bc5c160e453d5bc467694dac057e03d9e6c075a87bd6ff59be4ddedbfcb496168d9457e905edc3aff569270f0401560001f3d54fca1a7c3679ac631bd779d6  libsoup-3.4.0.tar.xz
c60d88791332da1218126bab0de7589e94e8e37d640f3729ddde6332369d87542f30c7e3101f79fb1091d2c2223f387e2b4c0257bcdf6397388dd10b0f5b8617  char-int-force-http.patch
"
