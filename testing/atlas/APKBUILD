# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=atlas
pkgver=0.10.0
pkgrel=0
pkgdesc="Database schema migration tool using modern DevOps principles"
url="https://atlasgo.io/"
# github.com/auxten/postgresql-parser fails to build on 32-bit platforms
# riscv64: fails to build
arch="all !x86 !armhf !armv7 !riscv64"
license="Apache-2.0"
makedepends="go sqlite-dev"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/ariga/atlas/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver/cmd/atlas"

export CGO_ENABLED=1 # required for sqlite driver
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o atlas \
		-tags "libsqlite3" \
		-ldflags "-X ariga.io/atlas/cmd/atlas/internal/cmdapi.version=v$pkgver"

	for shell in bash fish zsh; do
		./atlas completion $shell > atlas.$shell
	done
}

check() {
	go test ./...
}

package() {
	install -Dm755 atlas -t "$pkgdir"/usr/bin/

	install -Dm644 atlas.bash \
		"$pkgdir"/usr/share/bash-completion/completions/atlas
	install -Dm644 atlas.fish \
		"$pkgdir"/usr/share/fish/completions/atlas.fish
	install -Dm644 atlas.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_atlas
}

sha512sums="
7c223c18245a45bb1030e16e10f1e5c704fe42d6e94924a7be82ff0db6fdbb0ca8e236afe22dc9c2772cd288610005999666fc59a4f3cf248698f1aa6d6ae3f6  atlas-0.10.0.tar.gz
"
