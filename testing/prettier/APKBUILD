# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=prettier
pkgver=2.8.7
pkgrel=0
pkgdesc="Opinionated code formatter"
url="https://prettier.io/"
license="MIT"
# armhf, armv7, x86, riscv64: fails to build
arch="noarch !armhf !armv7 !x86 !riscv64"
depends="nodejs"
makedepends="yarn"
checkdepends="npm"
subpackages="$pkgname-doc"
source="https://github.com/prettier/prettier/archive/$pkgver/prettier-$pkgver.tar.gz
	timeout.patch
	"

build() {
	yarn install --frozen-lockfile
	yarn build
}

check() {
	yarn test:dist
}

package() {
	local destdir=/usr/lib/node_modules/prettier

	install -d \
		"$pkgdir"/usr/bin \
		"$pkgdir"/$destdir \
		"$pkgdir"/usr/share/licenses/prettier

	cp -r dist/* "$pkgdir"/$destdir
	ln -s $destdir/bin-prettier.js "$pkgdir"/usr/bin/prettier

	cd "$pkgdir"/$destdir
	rm -r esm # remove ES modules: not needed for command-line usage
	rm README.md # more links to various badges than actual content
	mv LICENSE "$pkgdir"/usr/share/licenses/prettier/LICENSE
}

sha512sums="
d6f556c40845f1376c64cd8b9cffb0d77595858bb6d9f8f08700dea42196c64e177bbb5067cb28052316ba2ca6d026baf37a5538f4355e581cdd3e659588543c  prettier-2.8.7.tar.gz
06968fc076d2cd68360601c84c49b82a1f872a1b54d5ec9738de7ac63793f96cd609817bbb0a0f1902b7ba004232d1276b3577557dc60dd2a7d71fad5e440099  timeout.patch
"
