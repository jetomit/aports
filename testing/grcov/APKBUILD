# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=grcov
pkgver=0.8.16
pkgrel=0
pkgdesc="Rust tool to collect and aggregate code coverage data"
url="https://github.com/mozilla/grcov"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le" # Limited by cargo
license="MPL-2.0"
depends="gcc" # gcov
makedepends="cargo"
options="net !check" # Failing due to utf-8 (locale?)
source="$pkgname-$pkgver.tar.gz::https://github.com/mozilla/grcov/archive/v$pkgver.tar.gz"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/grcov -t "$pkgdir"/usr/bin
}

sha512sums="
ef193fc5d3c71ea6c8be90b176eb735dc44a844d3fa547558fe8159ae6f31458c75c73533914233f35a23b3d3f2b5b435d69ff706f820b60447f72b3ef95c63e  grcov-0.8.16.tar.gz
"
