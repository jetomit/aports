# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libcap
pkgver=2.68
pkgrel=0
pkgdesc="POSIX 1003.1e capabilities"
arch="all"
license="BSD-3-Clause OR GPL-2.0-only"
url="https://sites.google.com/site/fullycapable/"
depends_dev="linux-headers"
makedepends_build="linux-headers perl bash"
makedepends_host="$depends_dev"
makedepends="$makedepends_build $makedepends_host"
subpackages="$pkgname-doc $pkgname-static $pkgname-dev $pkgname-utils libcap2"
source="https://kernel.org/pub/linux/libs/security/linux-privs/libcap2/libcap-$pkgver.tar"

build() {
	make BUILD_CC=gcc CC="${CC:-gcc}" lib=lib prefix=/usr GOLANG=no \
		DESTDIR="$pkgdir"
}

check() {
	make GOLANG=no test
}

package() {
	# backwards compatibility for things that depended on 'libcap'
	depends="libcap2=$pkgver-r$pkgrel $pkgname-utils=$pkgver-r$pkgrel"

	make lib=lib prefix=/usr GOLANG=no DESTDIR="$pkgdir" \
		install
}

utils() {
	pkgdesc="$pkgdesc (utils)"

	amove usr/sbin
}

libcap2() {
	replaces="$pkgname<2.64-r1"
	default_libs
}

sha512sums="
b5fe970e4e621ce33931013dccabdaf0face706148406f65d5791eed53755dc82edb7ceeec20499a684b8fa18e33c893e44d2ae01825431142cffa9e2a5032a3  libcap-2.68.tar
"
